### Проблема:
как "предсказать" score в поиске

---

1) Нужно посчитать feature-вектор между парами запрос - документ

2) Каждый элемент или группа элементов feature-вектора - это "быстрая" операция над другими векторами

---

```
  static final Map<String, DoubleBinaryOperator> SCALAR_OPERATIONS = ImmutableMap.<String, DoubleBinaryOperator>builder()
      .put("+", (x, y) -> x + y)
      .put("-", (x, y) -> x - y)
      .put("*", (x, y) -> x * y)
      .put("=", (x, y) -> VectorUtilities.isZero((float) (x - y)) ? 1.0f : 0.0f)
      .put("abs-", (x, y) -> Math.abs(x - y))
      .build();

  static final Map<String, ToDoubleBiFunction<Vector, Vector>> PAIRWISE_OPERATIONS = ImmutableMap.<String, ToDoubleBiFunction<Vector, Vector>>builder()
      .put("cos", Vector::cosine)
      .put("dot", Vector::dot)
      .put("bc", Vector::brayCurtis)
      .put("jaccard", Vector::jaccard)
      .put("dissim_left", Vector::dissimLeft)
      .put("dissim_right", Vector::dissimRight)
      .put("add_chi2", Vector::addChi2Kernel)
      .put("left_diff", Vector::leftOnlyDiff)
      .put("left_intersection", Vector::leftOnlyIntersect)
      .build();
```
---

3) Чтобы посчитать операции нужно уметь быстро получать векторные представления запрос/документ

---

доставка в поиск (online) векторов запроса
![online](images/online.png)

---
доставка в поиск (offline) векторов документов
![offline](images/offline.png)

---
4) Чтобы знать в каком порядке и какие операции применять для построения feature-вектора, а также какую ф-ю над ним применять нужно иметь конфигурацию, которую генерит сервис машинного обучения

---

```
    ...
    "model_ver": "master",
    "tree_filter": {
        "as_is": [
            {
                "key": "vacancy.tree_filter",
                "operand": "vacancy",
                "size": 1
            },
            {
                "key": "resume.tree_filter",
                "operand": "resume",
                "size": 3
            }
        ],
        "base_score": 0.0,
        "mailing_threshold": 0.65,
        "mobile_threshold": 0.65,
        "pairwise": [
            {
                "left_key": "employment.ohe",
                "left_operand": "vacancy",
                "op": "cos",
                "right_key": "by_vac.employment.ohe",
                "right_operand": "resume"
            },
    ...
```
---

описание деревянных моделей
```
booster[0]:
0:[f20<6.25468] yes=1,no=2,missing=1
	1:[f15<0.57862] yes=3,no=4,missing=4
		3:[f25<0.119517] yes=7,no=8,missing=7
			7:[f15<0.38227] yes=15,no=16,missing=16
				15:leaf=-0.574994
				16:leaf=-0.589186
			8:[f23<0.0756955] yes=17,no=18,missing=17
				17:leaf=-0.587314
				18:leaf=-0.59773
		4:[f13<0.619099] yes=9,no=10,missing=10
			9:[f21<4.10347] yes=19,no=20,missing=19
				19:leaf=-0.597725
				20:leaf=-0.593361
			10:[f21<7.70549] yes=21,no=22,missing=21
				21:leaf=-0.599222
				22:leaf=-0.59545
	2:[f15<0.496335] yes=5,no=6,missing=6
		5:[f25<0.119517] yes=11,no=12,missing=11
			11:[f23<0.0756955] yes=23,no=24,missing=23
				23:leaf=-0.536354
				24:leaf=-0.563388
booster[1]:
```

---
### Проблема: 
из-за нового экспериментального поиска 500-тит обычный поиск даже при включении на 5%

Решение: сделали отдельные индексы и вынесли их на отдельные машины

---

### Проблема: 
стало больше базовых поисков - индексация стала тупить, тк все были мастерами

Решение: сделали master-slave репликацию (с помощью lucene-replicator)

---


### Проблема: 
все тормозит, что именно непонятно

Решение: добавили метрики каждой стадии и кол-во документов, которые проходят каждую стадию

---

![angara_timings](images/angara_timings.png)

---

Не все "быстрые" операции оказались "быстрыми" - начали ускорять dot(): 
- кэширование норм векторов
- решение с бинарным поиском для несбалансированных по размеру sparse-векторов за O(n * log(m)) для n << m
- переиспользование уже вычисленных операций
- переиспользование обьектов (все что можно делаем мутабельным)
- и др.
---

![scalar_speedup](images/scalar_speedup.png)

---

Избавились от стадии десериализации векторов в онлайне*:
![rm_deserialization](images/rm_deserialization.png)

---
Добавили кэширование в бд векторов запроса*:
```
           50q       75q       90q       99q      avg(ms)
enable     232.0,    279.0,    352.0,    707.9,   227.68
disable    336.0,    418.0,    524.0,    860.4,   325.31
```

---

### Проблема: 
кэш в БД стал занимать много места

Решение: перестали кэшировать svd-вектора, которые получаем в онлайне в поиске умножением матрицы на другой вектор

---

Профит: экономия в ~4 раза
![precalculate](images/precalculate.png)

---

### Проблема:
индекс стал большим

Решение:
- Изначально сериализовали вектора в виде обьектов "как есть" - стали сохранять только то что нужно
- Перешли с double на float
---

### Проблема:
score в ml-сервисе и поиске не совпадают

---

Решение: подробный explain + debug url в ml-сервисе
![explain](images/explain.png)

---

### Проблема: 
ds хотят экспериментировать с порядком вычисления фич на том же множестве векторов

Решение: разделяем понятие "версия модели", которая зависит от индекса и "описание в каком порядке и какие операции нужно применять над векторами"

---

### Проблема:
ds делают новые модели/конфигурации и хотят удобно выкладывать эксперименты*

Решение: ml-сервис по http отдает конфигурации/модели, поиски в фоне подтягивают то, что им нужно

---
```
# получение актуальные моделей/сабмоделей
and.ivanov@oper:~$ curl -X POST 'http://192.168.2.199:3600/angara/versions_map'
{"master": ["master"], "truncated_log_reg": ["truncated_log_reg"]}

# получение json-конфига
and.ivanov@oper:~$ curl -X POST 'http://192.168.2.199:3600/angara/config?model_subver=truncated_log_reg'
{"linear": {"adv_threshold": 0.65, "as_is": [], "default_if_nan": 1.0, "mailing_threshold": 0.65, "mobile_threshold": 0.65, "pairwise": [{"left_key": "title.tfidf", "left_operand": "vacancy", "op": "cos", "right_key": "title.title.tfidf", "right_operand": "resume"}, {"left_key": "title.tfidf.svd", "left_operand": "vacancy", "op": "cos", "right_key": "title.title_and_position.tfidf.svd", "right_operand": "resume"}, {"left_key": "description.tfidf", "left_operand": "vacancy", "op": "cos", "right_key": "description.last_experience_description.tfidf", "right_operand": "resume"}, {"left_key": "specialization.ohe", "left_operand": "vacancy", "op": "cos", "right_key": "by_vac.specialization.ohe", "right_operand": "resume"}], "scalar": [], "threshold": 0.65, "threshold_variants": [{"mean_queue_size": 13.907438292194797, "mean_recall": 0.05, "queue_size_90_pct": 29.0, "threshold": -2.260214234127136}, {"mean_queue_size": 18.120246831220815, "mean_recall": 0.060000000000000005, "queue_size_90_pct": 41.0, "threshold": -2.3068342341274413}, {"mean_queue_size": 22.2628418945964, "mean_recall": 0.07, "queue_size_90_pct": 53.0, "threshold": -2.3433242341276803}, {"mean_queue_size": 26.963809206137427, "mean_recall": 0.08000000000000002, "queue_size_90_pct": 68.5, "threshold": -2.377014234127901}, {"mean_queue_size": 31.43178785857238, "mean_recall": 0.09000000000000001, "queue_size_90_pct": 80.0, "threshold": -2.4045942341280817}, {"mean_queue_size": 36.66144096064043, "mean_recall": 0.1, "queue_size_90_pct": 95.5, "threshold": -2.4321842341282625}, {"mean_queue_size": 42.327885256837895, "mean_recall": 0.11000000000000001, "queue_size_90_pct": 112.0, "threshold": -2.458954234128438}

# получение деревянного фильтра
and.ivanov@oper:~$ curl -X POST 'http://192.168.2.199:3600/angara/filter?model_subver=truncated_log_reg'
booster[0]:
0:[f20<5.29703] yes=1,no=2,missing=2
	1:[f18<0.610371] yes=3,no=4,missing=4
		3:[f23<0.110876] yes=7,no=8,missing=8
			7:[f10<0.999999] yes=15,no=16,missing=16
				15:leaf=-0.59799
				16:leaf=-0.583856
			8:[f17<0.883264] yes=17,no=18,missing=18
				17:leaf=-0.58721
				18:leaf=-0.59684
		4:[f21<2.44607] yes=9,no=10,missing=10

# получение ранкера
and.ivanov@oper:~$ curl -X POST 'http://192.168.2.199:3600/angara/ranker?model_subver=truncated_log_reg'
booster[0]:
0:[f58<-0.819656] yes=1,no=2,missing=1
	1:[f62<-1.69504] yes=3,no=4,missing=3
		3:[f54<-2.87039] yes=7,no=8,missing=7
			7:[f41<2.45329] yes=15,no=16,missing=15
				15:[f50<-3.01147] yes=31,no=32,missing=31
					31:[f549<-1.81619] yes=63,no=64,missing=63
```
---

### <a href="https://github.com/hhru/deploy-dev/commit/bc92d70016bb4367238b27cb09d28b2579401447" target="_blank">Так было:</a>
![diff_before](images/diff_before.png)


---

### <a href="https://github.com/hhru/deploy-dev/commit/672d0dd1788d5210a61465567b3e6526f20460f8" target="_blank">Так стало:</a>
![diff_after](images/diff_after.png)


---

### Проблема:
с ростом экспериментов стало сложно понимать, какие индексы, где лежат и т.д.

---
Решение: служебный url на поиске
```bash
and.ivanov@oper:~$ for host in s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 s14 s21 s22 s23 s24 s25 s27 s28 s29 s31 s32 s33 s34 s35; do echo $host; curl  "http://$host:9299/http/configuration"; done
s1
{"replicationRole":"SLAVE","indexes":["ANGARA"],"rankers":{"angara":["truncated_log_reg","master"]}}s2
{"replicationRole":"SLAVE","indexes":["ANGARA"],"rankers":{"angara":["truncated_log_reg","master"]}}s3
{"replicationRole":"SLAVE","indexes":["EMPLOYER","VACANCY"]}s4
{"replicationRole":"SLAVE","indexes":["YENISEI"],"rankers":{"yenisei":["watermarks","master"]}}s5
{"replicationRole":"SLAVE","indexes":["ANGARA"],"rankers":{"angara":["truncated_log_reg","master"]}}s6
{"replicationRole":"SLAVE","indexes":["ANGARA"],"rankers":{"angara":["truncated_log_reg","master"]}}s7
{"replicationRole":"MASTER_BACKUP","indexes":["ANGARA","ANGARA_TRUNCATED_LOG_REG","EMPLOYER","EMPLOYER_ARCHIVE","FAVORITE_RESUME_FOLDER","RESPONSE","RESUME","RESUME_ARCHIVE","RESUME_UNFINISHED","UNFILTERED_PROFILE","USER_ARCHIVE","VACANCY","VACANCY_ARCHIVE","VACANCY_CLOSED","WALRUS","YENISEI"]}s8
{"replicationRole":"SLAVE","indexes":["EMPLOYER","VACANCY"]}s9
{"replicationRole":"SLAVE","indexes":["YENISEI"],"rankers":{"yenisei":["watermarks","master"]}}s10
{"replicationRole":"SLAVE","indexes":["YENISEI"],"rankers":{"yenisei":["watermarks","master"]}}s11
{"replicationRole":"SLAVE","indexes":["YENISEI"],"rankers":{"yenisei":["watermarks","master"]}}s12
{"replicationRole":"SLAVE","indexes":["YENISEI"],"rankers":{"yenisei":["watermarks","master"]}}s13
{"replicationRole":"SLAVE","indexes":["EMPLOYER","VACANCY"]}s14
{"replicationRole":"SLAVE","indexes":["FAVORITE_RESUME_FOLDER","RESPONSE","RESUME"]}s21
{"replicationRole":"SLAVE","indexes":["FAVORITE_RESUME_FOLDER","RESPONSE","RESUME"]}s22
{"replicationRole":"SLAVE","indexes":["FAVORITE_RESUME_FOLDER","RESPONSE","RESUME"]}s23
{"replicationRole":"SLAVE","indexes":["FAVORITE_RESUME_FOLDER","RESPONSE","RESUME"]}s24
{"replicationRole":"SLAVE","indexes":["ANGARA_TRUNCATED_LOG_REG","EMPLOYER_ARCHIVE","RESUME_ARCHIVE","RESUME_UNFINISHED","USER_ARCHIVE","VACANCY_ARCHIVE","VACANCY_CLOSED"],"rankers":{"angara":["truncated_log_reg"]}}s25
{"replicationRole":"SLAVE","indexes":["FAVORITE_RESUME_FOLDER","RESPONSE","RESUME"]}s27
{"replicationRole":"SLAVE","indexes":["ANGARA_TRUNCATED_LOG_REG","EMPLOYER_ARCHIVE","RESUME_ARCHIVE","RESUME_UNFINISHED","USER_ARCHIVE","VACANCY_ARCHIVE","VACANCY_CLOSED"],"rankers":{"angara":["truncated_log_reg"]}}s28
{"replicationRole":"SLAVE","indexes":["ANGARA","ANGARA_TRUNCATED_LOG_REG"],"rankers":{"angara":["truncated_log_reg","master"]}}s29
{"replicationRole":"SLAVE","indexes":["ANGARA","ANGARA_TRUNCATED_LOG_REG"],"rankers":{"angara":["truncated_log_reg","master"]}}s31
{"replicationRole":"SLAVE","indexes":["UNFILTERED_PROFILE","WALRUS"]}s32
{"replicationRole":"SLAVE","indexes":["ANGARA"],"rankers":{"angara":["truncated_log_reg","master"]}}s33
{"replicationRole":"SLAVE","indexes":["UNFILTERED_PROFILE","WALRUS"]}s34
{"replicationRole":"MASTER","indexes":["ANGARA","ANGARA_TRUNCATED_LOG_REG","EMPLOYER","EMPLOYER_ARCHIVE","FAVORITE_RESUME_FOLDER","RESPONSE","RESUME","RESUME_ARCHIVE","RESUME_UNFINISHED","UNFILTERED_PROFILE","USER_ARCHIVE","VACANCY","VACANCY_ARCHIVE","VACANCY_CLOSED","WALRUS","YENISEI"],"rankers":{"angara":["truncated_log_reg","master"],"yenisei":["watermarks","master"]}}s35
{"replicationRole":"MASTER","indexes":["ANGARA","ANGARA_TRUNCATED_LOG_REG","EMPLOYER","EMPLOYER_ARCHIVE","FAVORITE_RESUME_FOLDER","RESPONSE","RESUME","RESUME_ARCHIVE","RESUME_UNFINISHED","UNFILTERED_PROFILE","USER_ARCHIVE","VACANCY","VACANCY_ARCHIVE","VACANCY_CLOSED","WALRUS","YENISEI"],"rankers":{"angara":["default","truncated_log_reg","master"],"yenisei":["master"]}
```

---
### <a href="https://wiki.hh.ru/display/EXP/search" target="_blank">lssearch</a>
![lssearch](images/lssearch.png)

---
Хотим использовать в ранжировании то, что считает lucene

### Проблема:
сложно оттуда вытащить статистики

Решение: оверрайдим все что можно, чтобы достать подобные статистики (lucene features)

---

```
List<Similarity> similarities = new ArrayList<>();
    similarities.add(new DefaultSimilarity());
    similarities.add(new BM25Similarity());
    similarities.add(new BM25Similarity(1.2f, 0));
    similarities.add(new BM25Similarity(1.2f, 1));

    for (BasicModel basicModel : BASIC_MODELS) {
      for (AfterEffect afterEffect : AFTER_EFFECTS) {
        for (Normalization normalization : NORMALIZATIONS) {
          similarities.add(new DFRSimilarity(basicModel, afterEffect, normalization));
        }
      }
    }

    for (Distribution distribution : DISTRIBUTIONS) {
      for (Lambda lambda : LAMBDAS) {
        for (Normalization normalization : NORMALIZATIONS) {
          similarities.add(new IBSimilarity(distribution, lambda, normalization));
        }
      }
    }

    similarities.add(new LMDirichletSimilarity());
    similarities.add(new LMJelinekMercerSimilarity(new LMSimilarity.DefaultCollectionModel(), 0.1F));
    similarities.add(new LMJelinekMercerSimilarity(new LMSimilarity.DefaultCollectionModel(), 0.3F));
    similarities.add(new LMJelinekMercerSimilarity(new LMSimilarity.DefaultCollectionModel(), 0.5F));
    similarities.add(new LMJelinekMercerSimilarity(new LMSimilarity.DefaultCollectionModel(), 0.7F));
```

